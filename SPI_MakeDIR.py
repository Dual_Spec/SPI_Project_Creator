#Takes the title from the project manager's email title then copies the default project file
#from the templates directory and formats the aep filename as well based on that title.

import shutil
import os

proj_name = input('Project Name: ')

src_template_gbh_give = '/mnt/d/_PROJECTS/SPI Motion Graphics Work/_TEMPLATES/GBH_Generic_Comp_Template'
src_template_ts_give = '/mnt/d/_PROJECTS/SPI Motion Graphics Work/_TEMPLATES/TS_Generic_Comp_Template'
src_template_sccc_give = '/mnt/d/_PROJECTS/SPI Motion Graphics Work/_TEMPLATES/SCCC_Giveaway_Template'
src_template_generic = '/mnt/d/_PROJECTS/SPI Motion Graphics Work/_TEMPLATES/SPI_Generic_Comp_Template'
src_template_dm_winning ='/mnt/d/_PROJECTS/SPI Motion Graphics Work/_TEMPLATES/DM_Winning_Wall_and_Landscape_Template' 
dest_default = '/mnt/c/Users/sarks/DRIVE_MIRROR/_PROJECTS/SPI Motion Graphics Work/' + proj_name.replace(' ', '_')


def main():

    choice ='0'

    while choice =='0':
        print("1 Default SPI")
        print("2 DM Winnning Wall")
        print("3 SCCC Giveaway")
        print("4 GBH Giveaway")
        print("5 TS Events")
        choice = input(' ')

        if choice == "1":
            shutil.copytree(src_template_generic, dest_default)
            os.rename(dest_default + '/' + 'SPI_Generic_Comp_Template.aep', dest_default + '/' + proj_name.replace(' ', '_') + ".aep")
            break

        if choice == "2":
            shutil.copytree(src_template_dm_winning, dest_default)
            os.rename(dest_default + '/' + 'SPI_Generic_Comp_Template.aep', dest_default + '/' + proj_name.replace(' ', '_') + ".aep")
            break

        if choice == "3":
            shutil.copytree(src_template_sccc_give, dest_default)
            os.rename(dest_default + '/' + 'SPI_Generic_Comp_Template.aep', dest_default + '/' + proj_name.replace(' ', '_') + ".aep")
            break

        if choice == "4":
            shutil.copytree(src_template_gbh_give, dest_default)
            os.rename(dest_default + '/' + 'SPI_Generic_Comp_Template.aep', dest_default + '/' + proj_name.replace(' ', '_') + ".aep")
            break

        if choice == "5":
            shutil.copytree(src_template_ts_give, dest_default)
            os.rename(dest_default + '/' + 'SPI_Generic_Comp_Template.aep', dest_default + '/' + proj_name.replace(' ', '_') + ".aep")
            break

        else:
            print("Enter Again: ")


main()
